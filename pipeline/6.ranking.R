####### ranking.R


########### ranking candidate sgRNAs for sgRNA selection #########

setwd("")

outputFolder<-"Format"

dataFile<-read.table(paste(outputFolder,"/sgRNA_geneSelect_Lib.txt",sep=""),
                     header=TRUE,sep=",",fill=TRUE)



fastaMatrix<-character(2*nrow(dataFile))

for(i in 1:nrow(dataFile)){
  
  fastaMatrix[2*i-1]<-paste(">",paste(dataFile[i,1],dataFile[i,2],dataFile[i,3],dataFile[i,4],dataFile[i,5],sep="_"),sep="")
  fastaMatrix[2*i]<-as.character(dataFile[i,5])
}

write.table(fastaMatrix,
            paste(outputFolder,"/sgRNA_geneSelect_Lib.fasta",sep=""),
            append = FALSE, quote = FALSE, sep = "",
            row.names = FALSE, col.names = TRUE)
############### optional: check sgRNA off-target with mismatches

outputFolder<-"Format"

mapFile<-read.table(paste(outputFolder,"/sgRNA_geneSelect_Lib.fasta.out",sep=""),
                     header=TRUE,sep="",fill=TRUE)

sgTable<-as.matrix(table(mapFile[,1]))

multi<-sgTable[which(as.numeric(sgTable[,1])>1),1]

dataFile<-read.table(paste(outputFolder,"/sgRNA_geneSelect_Lib.txt",sep=""),
                     header=TRUE,sep=",",fill=TRUE)


sgId<-paste(dataFile[,1],dataFile[,2],dataFile[,3],dataFile[,4],dataFile[,5],sep="_")

multiNames<-names(multi)

hits<-rep(1,length(sgId))

for(i in 1:length(multiNames)){
  
  tmpindex<-which(sgId==multiNames[i])
  
  hits[tmpindex]<-as.numeric(multi[i])
  
}

filter<-which(nchar(as.character(dataFile[,"chr"]))<6)


write.table(cbind(dataFile[filter,],hits[filter]),
            paste(outputFolder,"/sgRNA_geneSelect_Lib_mismatch.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = TRUE)

###########################################################

setwd("")

outputFolder<-"Format"

dataFile<-read.table(paste(outputFolder,"/sgRNA_geneSelect_Lib_mismatch.txt",sep=""),
                     header=TRUE,sep=",",fill=TRUE)
                     
refFile<-read.table(paste(outputFolder,"/refGeneInfo.txt",sep=""),
                    header=TRUE,sep="",fill=TRUE)


scoreMatrix<-matrix(0,nrow=nrow(dataFile),ncol=7)

tgPosList<-as.numeric(dataFile[,4])

geneList<-as.character(dataFile[,1])

geneStrand<-as.character(refFile[,3])

refGeneList<-as.character(refFile[,"name2"])

txStart<-as.numeric(refFile[,"txStart"] )

txEnd<-as.numeric(refFile[,"txEnd"] )

exonSt<-as.character(refFile[,"exonStarts"])

exonEnd<-as.character(refFile[,"exonEnds"])

strand<-as.character(dataFile[,3])

type<-as.numeric(dataFile[,6])

seqList<-as.character(dataFile[,5])

### polyN score 
polyN<-as.numeric(lapply(substr(seqList,1,20), regexpr, pattern="GGGG"))+
  as.numeric(lapply(substr(seqList,1,20), regexpr, pattern="AAAA"))+
  as.numeric(lapply(substr(seqList,1,20), regexpr, pattern="CCCC"))
  
iniScore=polyNScore=GCScore=tierScore=infrmExScore<-rep(0,length(polyN))

polyNScore[which(polyN>-3)]<--1   ################


### GC score
GCCheck<-as.numeric(lapply(substr(seqList,3,5), regexpr, pattern="GC")) + 
  as.numeric(lapply(substr(seqList,7,8), regexpr, pattern="GC"))

GCScore[which(GCCheck>-2)]<--5   ##################

mismatch<-(as.numeric(dataFile[,"hits"])-1)*(-1)



for(i in 1:length(type)){
  
  if(type[i]==0){
    
    iniScore[i]<-10
    ####++++++++++++++ same process with type 3 ++++++++++++++++++

    idx<-which(refGeneList %in% geneList[i])
    
	### select the smallest transcript of individual genes for calculating sgRNA target relative location
    smallTx<-which.min(txEnd[idx]-txStart[idx])   
    gstart<-txStart[idx[smallTx]]   
    gend<-txEnd[idx[smallTx]]   
    gstrand<-geneStrand[idx[smallTx]]
    
    tgPos<-tgPosList[i]  
    infrmEx<-0
    
	##### calculate sgRNA target location along gene 
    if(gstrand=="+"){
      
      tier<-round((tgPos-gstart)/((gend-gstart)*0.125),2)
      
      tmpExonSt<-unlist(strsplit(exonSt[idx[smallTx]],","))
      tmpExonEnd<-unlist(strsplit(exonEnd[idx[smallTx]],","))
      
      exonIdx<-which(tmpExonSt==tgPos)
      
      if(length(exonIdx)>0){
        
		### check skipped inframe exons by SS sgRNA 
        if((as.numeric(tmpExonEnd[exonIdx])-as.numeric(tmpExonSt[exonIdx]))%%3==0){
          
          infrmEx<-1
        }
        
      }
      
    }else{
      
      tier<-round((gend-tgPos)/((gend-gstart)*0.125),2)
      
      tmpExonSt<-unlist(strsplit(exonEnd[idx[smallTx]],","))
      tmpExonEnd<-unlist(strsplit(exonSt[idx[smallTx]],","))
      
      exonIdx<-which(tmpExonSt==tgPos)
      
      if(length(exonIdx)>0){
        
        if((as.numeric(tmpExonEnd[exonIdx])-as.numeric(tmpExonSt[exonIdx]))%%3==0){
          
          infrmEx<-1
        }
        
      }
      

      
    }
    
    if(tier>=0 & tier<=8){  #############
      
      tierScore[i]<-0-tier
      
    }else{
      
      tierScore[i]<--9   #################
      
    }
    
    
    if(infrmEx==1) infrmExScore[i] <--2   ################
    
    
    ##+++++++++++++++++++++++++++same process with type 3+++++++++++++++++++++++++++++++++++
    
  }
  
  
  if(type[i]==1){
    
    iniScore[i]<-10
    
    idx<-which(refGeneList %in% geneList[i])
    
    smallTx<-which.min(txEnd[idx]-txStart[idx])
    
    gstart<-txStart[idx[smallTx]]
    
    gend<-txEnd[idx[smallTx]]
    
    gstrand<-geneStrand[idx[smallTx]]
    
    if(gstrand=="+"){
      tgPos<-tgPosList[i]+6
      
      tier<-round((tgPos-gstart)/((gend-gstart)*0.125),2)
      
    }else{
      
      tgPos<-tgPosList[i]+18
      
      tier<-round((gend-tgPos)/((gend-gstart)*0.125),2)
      
    }
    
    if(tier>=0 & tier<=8){  ##############
      
      tierScore[i]<-0-tier
      
    }else{
      
      tierScore[i]<--9   #################
      
    }
    
  }
  
  if(type[i]==2){
    
    iniScore[i]<-10
    
    
    
  }
  
  if(type[i]==3){
    
    iniScore[i]<-0
    
    ####++++++++++++++ same process with type 3 ++++++++++++++++++
    
    
    idx<-which(refGeneList %in% geneList[i])
    
    smallTx<-which.min(txEnd[idx]-txStart[idx])
    
    gstart<-txStart[idx[smallTx]]
    
    gend<-txEnd[idx[smallTx]]
    
    gstrand<-geneStrand[idx[smallTx]]
    
    tgPos<-tgPosList[i]
    
    infrmEx<-0
    
    if(gstrand=="+"){
      
      tier<-round((tgPos-gstart)/((gend-gstart)*0.125),2)
      
      tmpExonSt<-unlist(strsplit(exonSt[idx[smallTx]],","))
      tmpExonEnd<-unlist(strsplit(exonEnd[idx[smallTx]],","))
      
      exonIdx<-which(tmpExonSt==tgPos)
      
      if(length(exonIdx)>0){
        
        if((as.numeric(tmpExonEnd[exonIdx])-as.numeric(tmpExonSt[exonIdx]))%%3==0){
          
          infrmEx<-1
        }
        
      }
      
    }else{
      
      tier<-round((gend-tgPos)/((gend-gstart)*0.125),2)
      
      tmpExonSt<-unlist(strsplit(exonEnd[idx[smallTx]],","))
      tmpExonEnd<-unlist(strsplit(exonSt[idx[smallTx]],","))
      
      exonIdx<-which(tmpExonSt==tgPos)
      
      if(length(exonIdx)>0){
        
        if((as.numeric(tmpExonEnd[exonIdx])-as.numeric(tmpExonSt[exonIdx]))%%3==0){
          
          infrmEx<-1
        }
        
      }
      
      
      
    }
    
    if(tier>=0 & tier<=8){  #############
      
      tierScore[i]<-0-tier
      
    }else{
      
      tierScore[i]<--9   #################
      
    }
    
    
    if(infrmEx==1) infrmExScore[i] <--2   ################
    
    
    ##+++++++++++++++++++++++++++same process with type 3+++++++++++++++++++++++++++++++++++
    
    
  }
  
  if(type[i]==4){
    
    iniScore[i]<--10
    
    
    
  }
  
  
  
}

sumScore<-round(rowSums(cbind(iniScore,tierScore,infrmExScore,polyNScore,GCScore,mismatch)),2)

output<-cbind(iniScore,tierScore,infrmExScore,polyNScore,GCScore,mismatch,sumScore,dataFile)

geneList<-as.character(dataFile[,"gene"])

sgCount<-table(geneList)

defGene<-names(which(sgCount<3))

filter<-which(!geneList %in% defGene)


write.table(output[filter,],
            paste(outputFolder,"/sgRNA_rankScore.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = TRUE)




################# select top3 sgRNA for individual gene ########
setwd("")

outputFolder<-"Format"

dataFile<-read.table(paste(outputFolder,"/sgRNA_rankScore_ccdsMod_30cdsOF.txt",sep=""),
                                          header=TRUE,sep=",",fill=TRUE)

geneList<-as.character(dataFile[,"gene"])

sgCount<-table(geneList)

length(which(sgCount>=3))

geneNames<-names(sgCount)

triSgMatrix<-matrix(ncol=ncol(dataFile),nrow=length(geneNames)*3)

for(i in 1:length(geneNames)){
  
  gene<-geneNames[i]
  
  idx<-which(geneList %in% gene)
  
  sgScore<-dataFile[idx,"sumScore"]
  
  ### if candidate sgRNA number is 3 ########
  if(length(sgScore)==3 ){
    
    triSgMatrix[c((i*3-2):(i*3)),]<-as.matrix(dataFile[idx,])
    
  } else {
    
    tmp<-unique(sgScore)
    
    scoreRank<-tmp[order(tmp,decreasing = TRUE)]
    
    sgSelect<-rep(0,3)
    
	######### if sgRNAs have same ranking score #########
    if(length(tmp)<3){
      
      triSgMatrix[c((i*3-2):(i*3)),]<-as.matrix(dataFile[idx[c(1,ceiling(length(sgScore)/2),length(sgScore))],])
      
      
    }else{
      
	  ######### if sgRNAs have same ranking score and pick up sgRNA by target C loction at editing window #########
      for(ss in 1:3){
        
        sSelect<-which(sgScore==scoreRank[ss])
        
        if(length(sSelect)>1){
          
          tmpPos<-which(substr(as.character(dataFile[idx[sSelect],"seq"]),6,6)=="C")
          
          if(length(tmpPos)>0){
            
            triSgMatrix[(i*3-3+ss),]<-as.matrix(dataFile[idx[sSelect[tmpPos[1]]],])
            
          }else{
            
            tmpPos<-which(substr(as.character(dataFile[idx[sSelect],"seq"]),7,7)=="C")
            
            if(length(tmpPos)>0){
              
              triSgMatrix[(i*3-3+ss),]<-as.matrix(dataFile[idx[sSelect[tmpPos[1]]],])
              
            }else{
              
              triSgMatrix[(i*3-3+ss),]<-as.matrix(dataFile[idx[sSelect[1]],])
              
            }
            
          }
          
        }
        if(length(sSelect)==1){
          
          triSgMatrix[(i*3-3+ss),]<-as.matrix(dataFile[idx[sSelect],])
          
        }
        
      }  #### for ss over
      
    } ### unique score>=3 over
    
    
  } #### sgrna count>3 over
  
  
} #### for gene list over


write.table(triSgMatrix,
            paste(outputFolder,"/sgRNA_tri_select_cdsMod.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = names(dataFile))



################ format selected sgRNA infomation ############

setwd("")

outputFolder<-"Format"

libFile<-read.table(paste(outputFolder,"/sgRNA_tri_select_cdsMod.txt",sep=""),
                    header=TRUE,sep=",",fill=TRUE)

geneList<-names(table(as.character(libFile[,"gene"])))


type<-as.character(libFile[,"type"])


seq<-substr(as.character(libFile[,"seq"]),1,20)

pam<-substr(as.character(libFile[,"seq"]),21,23)

write.table(cbind(libFile[,"gene"],seq,pam,libFile[,c(9,10,11,13)]),
            "sgRNA_finalCheck.txt",
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = c("gene","seq","pam","chr","t.strand","t.loc","type"))




