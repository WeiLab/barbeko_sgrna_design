#### 3.offTargetCheck.R

######### prepare fasta of candidate sgRNA ###########

outputFolder<-"OffTarget"

dataFile<-read.table(paste(outputFolder,"/SgRNASeq_NGGRev.txt",sep=""),
                     header=TRUE,sep=",",fill=TRUE)



fastaMatrix<-character(2*nrow(dataFile))

for(i in 1:nrow(dataFile)){
  
  fastaMatrix[2*i-1]<-paste(">",paste(dataFile[i,1],dataFile[i,2],dataFile[i,3],dataFile[i,4],dataFile[i,9],sep="_"),sep="")
  fastaMatrix[2*i]<-as.character(dataFile[i,9])
}

write.table(fastaMatrix,
            paste(outputFolder,"/SgRNASeq_NGGRev.fasta",sep=""),
            append = FALSE, quote = FALSE, sep = "",
            row.names = FALSE, col.names = TRUE)


########## after whole genome mapping of candidate sgRNA sequences ,remove candidate sgRNAs of multiple hits ##############


outputFolder<-"OffTarget"

dataFile<-read.table(paste(outputFolder,"/SgRNASeq_NAGRev.fasta.out",sep=""),
                     header=FALSE,sep="",fill=TRUE)

sgTable<-table(dataFile[,1])


uniqueSeq<-which(as.numeric(sgTable)==1)

length(table(dataFile[,1]))

uniqueSgList<-names(sgTable)[uniqueSeq]

seqFile<-read.table(paste(outputFolder,"/SgRNASeq_NAGRev.txt",sep=""),
                    header=TRUE,sep=",",fill=TRUE)

seqId<-paste(seqFile[,1],seqFile[,2],seqFile[,3],seqFile[,4],seqFile[,9],sep="_")

uniqueIndex<-which(seqId %in% uniqueSgList)


write.table(seqFile[uniqueIndex,],
            paste(outputFolder,"/sgRNANAG_OT_Check.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = TRUE)



