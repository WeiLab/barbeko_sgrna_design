##### 5.format.R


##### format different types of sgRNAs and make sgRNAs target same gene list 


### format SS sgRNA #######

outputFolder<-"Format"

sgRNAFile<-read.table(paste(outputFolder,"/sgRNANGG_OT_Check_polyT_GC.txt",sep=""),
                      header=TRUE,sep=",",fill=TRUE)

geneList<-read.table(paste(outputFolder,"/geneName2.txt",sep=""),
                     header=FALSE,sep="",fill=TRUE)

tmp<-sgRNAFile[,1] %in% geneList[,1]

cover<-table(sgRNAFile[tmp,1])


write.table(sgRNAFile[tmp,c(1:4,9)],
            paste(outputFolder,"/ssNGGLib.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = TRUE)


### format STOP sgRNA ####### 

outputFolder<-"Format"

sgRNAFile<-read.table(paste(outputFolder,"/sgAllStop_Check_polyT_GC.txt",sep=""),
                      header=TRUE,sep=",",fill=TRUE)

geneList<-read.table(paste(outputFolder,"/geneName2.txt",sep=""),
                     header=FALSE,sep="",fill=TRUE)

tmp<-sgRNAFile[,9] %in% geneList[,1]

sum(tmp)

cover<-table(sgRNAFile[tmp,9])

write.table(sgRNAFile[tmp,c(9,7,6,8,5)],
            paste(outputFolder,"/allStopLib.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = TRUE)

### format start codon sgRNA ####### 
outputFolder<-"Format"

sgRNAFile<-read.table(paste(outputFolder,"/startCodon_SgRNA_NGG_OT_polyT_GC.txt",sep=""),
                      header=TRUE,sep=",",fill=TRUE)



geneList<-read.table(paste(outputFolder,"/geneName2.txt",sep=""),
                     header=FALSE,sep="",fill=TRUE)

tmp<-sgRNAFile[,3] %in% geneList[,1]

cover<-table(sgRNAFile[tmp,1])


write.table(sgRNAFile[tmp,c(3,8,9,7,1)],
            paste(outputFolder,"/scNGGLib.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = TRUE)


############### combine different types of sgRNAs #####

outputFolder<-"Combine"

file1<-read.table(paste(outputFolder,"/ssNGGLib.txt",sep=""),
                  header=TRUE,sep=",",fill=TRUE)

file2<-read.table(paste(outputFolder,"/allStopLib.txt",sep=""),
                  header=TRUE,sep=",",fill=TRUE)


file3<-read.table(paste(outputFolder,"/scNGGLib.txt",sep=""),
                  header=TRUE,sep=",",fill=TRUE)


file4<-read.table(paste(outputFolder,"/ssNAGLib.txt",sep=""),
                  header=TRUE,sep=",",fill=TRUE)



file5<-read.table(paste(outputFolder,"/scNAGLib.txt",sep=""),
                  header=TRUE,sep=",",fill=TRUE)





write.table(rbind(as.matrix(cbind(file1,rep(0,nrow(file1)))),
                  as.matrix(cbind(file2,rep(1,nrow(file2)))),
                  as.matrix(cbind(file3,rep(2,nrow(file3)))),
                  as.matrix(cbind(file4,rep(3,nrow(file4)))),
                  as.matrix(cbind(file5,rep(4,nrow(file5))))),
            paste(outputFolder,"/ssNGGNAGscNGGNAGStopLib.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = TRUE)


################

outputFolder<-"Combine"

combineFile<-read.table(paste(outputFolder,"/ssNGGNAGscNGGNAGStopLib.txt",sep=""),
                        header=TRUE,sep=",",fill=TRUE)

nrow(combineFile)

tmp<-table(combineFile[,1])

length(tmp)

length(which(tmp>2))

table(as.character(combineFile[,6]))

length(which(tmp>3))

################### prepare fasta of candidate sgRNA sequence for whole gonome mapping ##############

setwd("")

outputFolder<-"Combine"

combineFile<-read.table(paste(outputFolder,"/ssNGGNAGscNGGNAGStopLib.txt",sep=""),
                        header=TRUE,sep=",",fill=TRUE)


fastaMatrix<-character(2*nrow(combineFile))

for(i in 1:nrow(combineFile)){
  
  fastaMatrix[2*i-1]<-paste(">",paste(combineFile[i,1],combineFile[i,2],combineFile[i,3],combineFile[i,4],combineFile[i,5],sep="_"),sep="")
  fastaMatrix[2*i]<-as.character(combineFile[i,5])
}

write.table(fastaMatrix,
            paste(outputFolder,"/combine_seq.fasta",sep=""),
            append = FALSE, quote = FALSE, sep = "",
            row.names = FALSE, col.names = TRUE)


##########


outputFolder<-"Combine"

mapFile<-read.table(paste(outputFolder,"/combine_seq.fasta.out",sep=""),
                    header=FALSE,sep="",fill=TRUE)

combFile<-read.table(paste(outputFolder,"/ssNGGNAGscNGGNAGStopLib.txt",sep=""),
                     header=TRUE,sep=",",fill=TRUE)


write.table(cbind(combFile,mapFile[,2:4]),
            paste(outputFolder,"/combine_seq_map.txt",sep=""),
            append = FALSE, quote = FALSE, sep = ",",
            row.names = FALSE, col.names = TRUE)



