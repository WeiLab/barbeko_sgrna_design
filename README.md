# README #

##################################################################
This file descripts the processes of designing sgRNAs of BARBEKO in details.  
##################################################################

################## I. Design sgRNAs for consensus splicing sites. ###############

	The UCSC Table Browser (https://genome.ucsc.edu/cgi-bin/hgTables) was used for identifying the splice sites
	Select Fields from hg38.knownGene  
exonStarts
Exon start positions (or end positions for minus strand item)
exonEnds
Exon end positions (or start positions for minus strand item)
	Confirm the “AG-GT” splice site pattern near the annotated positions.
Script@ 1.extSeq.R

	Extract the reverse sequences (approximately 28nt) around splice sites which allow the targeted “C” locating at the 4th to 8th positions of sgRNAs.
	Search PAM and construct candidate sgRNAs.
Script@ 2.searchPAM.R

	Map sgRNA sequences to the whole genome by bowtie-1.2.1.1 using index “GCA_000001405.15_GRCh38_no_alt_analysis_set”.
	Remove the sgRNAs that have multiple hits in the mapping results (No concerning about mismatches to maximize the number of sgRNAs).
Script@ 3.offTargetCheck.R

	Remove sgRNAs that have “TTTT”.
	Remove sgRNAs with GC contents beyond 0.2-0.8.
Script@ 4.onTargetCheck.R

	Format sgRNA sequences.
Script@ 5.format.R

	Rank different sgRNAs of same gene.
Script@ 6.ranking.R
ScoreTable@


 

##################   II. Design sgRNAs for start codon.    #################
	Annotation (ftp://ftp.ncbi.nlm.nih.gov/pub/CCDS/current_human)
CDS positions@ CCDS.20160908.txt
CDS sequences@ CCDS_nucleotide.20160908.fna
	Extract sequences of first exons across different genes from CCDS sequence annotation.
Script@ a.1.CCDSCheck.R

	Search “ATG” along the first exons and remove the start codons with an in-frame “ATG” downstream.
	Search PAM in the reversed strand of start codon.
Script@ a.2.startSearchPAM.R

	Off-target check, on-target efficiency check, format and ranking sgRNAs were same as designing sgRNAs of splice sites.
Script@ a.3.offTargetSC.R

##################   III. Design sgRNAs for AAVS1.    #################
	The annotation of AAVS1 is given by https://www.ncbi.nlm.nih.gov/nuccore/261958, but the annotated sequence is only 99% mapping to the genome sequences of hg38.
	Genome sequence of chr19: 55113873- 55117983 is used as AAVS1.
	All “C” in the double strands of AAVS1 were designed as sgRNA targets.
Script@ a.4.AAVS1sg.R


##################   IV. Generate non-target control    #################
	Randomly generate 20nt sequences
	Map sequences to whole genome and select the non-targeting sequences.
Script@ 7.negControl.R
